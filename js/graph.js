

/**
 * 
 * @param {SVGElement} svg 
 * @param {string} url 
 */
function getGraphData(svg, url, currency) {
    $.getJSON(url, function (data) {
        console.log("TAA", data);
        var average = 0;
        var hi = 0;
        var low = 100000;
        data.Data.forEach(function (dat) {
            average += dat.close;
            if (dat.close > hi) hi = dat.close;
            if (dat.close < low) low = dat.close;
        }, this);

        average = average / 25;
        function scaleBetween(unscaledNum, minAllowed, maxAllowed, min, max) {
            return (maxAllowed - minAllowed) * (unscaledNum - min) / (max - min) + minAllowed;
        }
        var path = "M 50," + data.Data[0].close;
        var x = 35;
        data.Data.forEach(function (d) {
            var y = 100 - scaleBetween(d.close,0,50, low, hi);//120 - (Math.pow(d.close / hi, average) + 20);
            x += 344 / 24;
            path += " L" + x + "," + y;// (((y * 200)-300)*-1);
        });
        path += "L 1000,1000";
        path += "Z";
        svg.contentDocument.getElementById("graph").setAttribute("d", path);
        svg.contentDocument.getElementById("price").textContent = "$ " + data.Data[data.Data.length - 1].close;
        svg.contentDocument.getElementById("currency").textContent = currency;
        svg.contentDocument
        var yesterday = data.Data[0].close;
        var today = data.Data[data.Data.length - 1].close;
        svg.contentDocument.getElementById("percent").textContent = "";// "%"+(((today * 100) / yesterday) - 100).toFixed(2);
        if (today < yesterday) {
            svg.contentDocument.getElementById("percent").style.fill = "red";
            svg.contentDocument.getElementById("graph").style.fill = "#da7171";
            svg.contentDocument.getElementById("graph").style.stroke = "red";
            svg.contentDocument.getElementById("footer").style.fill = "darkred";
        } else {
            svg.contentDocument.getElementById("percent").style.fill = "green";
            svg.contentDocument.getElementById("graph").style.fill = "#8fbc8f";
            svg.contentDocument.getElementById("graph").style.stroke = "green";
            svg.contentDocument.getElementById("footer").style.fill = "green";
        }


        console.log(svg.contentDocument.getElementById("graph"));
    })
}
setTimeout(function () {
    getGraphData(document.getElementById("btcUsd"), "https://min-api.cryptocompare.com/data/histohour?aggregate=1&e=CCCAGG&extraParams=CryptoCompare&fsym=BTC&limit=24&tryConversion=false&tsym=USD", "BTC - USD");
    getGraphData(document.getElementById("ethUsd"), "https://min-api.cryptocompare.com/data/histohour?aggregate=1&e=CCCAGG&extraParams=CryptoCompare&fsym=ETH&limit=24&tryConversion=false&tsym=USD", "ETH - USD");
    getGraphData(document.getElementById("dashUsd"), "https://min-api.cryptocompare.com/data/histohour?aggregate=1&e=CCCAGG&extraParams=CryptoCompare&fsym=DASH&limit=24&tryConversion=false&tsym=USD", "DASH - USD");
    getGraphData(document.getElementById("zecUsd"), "https://min-api.cryptocompare.com/data/histohour?aggregate=1&e=CCCAGG&extraParams=CryptoCompare&fsym=ZEC&limit=24&tryConversion=false&tsym=USD", "ZEC - USD");
}, 1000);


var socket = io.connect('https://streamer.cryptocompare.com/');
var _oldPriceOne = 0;
var _oldPriceTwo = 0;
var _oldPriceThree = 0;
var _oldPriceFour = 0;
var _oldPriceFive = 0;
var _oldPriceSix = 0;
//Format: {SubscriptionId}~{ExchangeName}~{FromSymbol}~{ToSymbol}
//Use SubscriptionId 0 for TRADE, 2 for CURRENT and 5 for CURRENTAGG
//For aggregate quote updates use CCCAGG as market
var subscription = ['5~CCCAGG~BTC~USD', '5~CCCAGG~ETH~USD', '5~CCCAGG~DASH~USD', '5~CCCAGG~ZEC~USD', '5~CCCAGG~BTC~EUR', '5~CCCAGG~ETH~EUR', '5~CCCAGG~ETH~BTC', '5~CCCAGG~ZEC~BTC', '5~CCCAGG~DASH~BTC'];

socket.emit('SubAdd', { subs: subscription });

socket.on("m", function (message) {
    var messageType = message.substring(0, message.indexOf("~"));
    var res = {};
    if (messageType === CCC.STATIC.TYPE.CURRENTAGG) {
        res = CCC.CURRENT.unpack(message);
        if(res.TOSYMBOL == "USD" && res.FROMSYMBOL == "BTC")
        console.log(res);
        if(res.TOSYMBOL == "USD")
            window[res.FROMSYMBOL](res);
        //updateQuote(res);
        //enmanuel space
        if (res.FROMSYMBOL == "BTC" && res.TOSYMBOL == "EUR") {
            if (res.PRICE) {
                if (_oldPriceOne != 0) {

                    if (_oldPriceOne > res.PRICE) {
                        document.getElementById('BTC_EUR_CLOSED').style.transition = 'ease';
                        document.getElementById('BTC_EUR_CLOSED').style.color = '#ff4c4c';
                    } else {
                        document.getElementById('BTC_EUR_CLOSED').style.color = '#77CB8E';
                    }
                }
            }
            if (res.PRICE) {

                document.getElementById('BTC_EUR_CLOSED').textContent = '€ ' + res.PRICE.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
                _oldPriceOne = res.PRICE;
            }
            if (res.VOLUME24HOURTO) {
                document.getElementById('BTC_EUR_VOLUME').textContent = 'V: € ' + res.VOLUME24HOURTO.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
            }

        }

        if (res.FROMSYMBOL == "ETH" && res.TOSYMBOL == "EUR") {
            if (res.PRICE) {
                if (_oldPriceTwo != 0) {

                    if (_oldPriceTwo > res.PRICE) {
                        document.getElementById('ETH_EUR_CLOSED').style.transition = 'ease';
                        document.getElementById('ETH_EUR_CLOSED').style.color = '#ff4c4c';
                    } else {
                        document.getElementById('ETH_EUR_CLOSED').style.color = '#77CB8E';
                    }
                }
            }
            if (res.PRICE) {
                document.getElementById('ETH_EUR_CLOSED').textContent = '€ ' + res.PRICE.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
                _oldPriceTwo = res.PRICE;
            }
            if (res.VOLUME24HOURTO) {
                document.getElementById('ETH_EUR_VOLUME').textContent = 'V: € ' + res.VOLUME24HOURTO.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
            }
        }

        if (res.FROMSYMBOL == "ETH" && res.TOSYMBOL == "BTC") {
            if (res.PRICE) {
                if (_oldPriceThree != 0) {

                    if (_oldPriceThree > res.PRICE) {
                        document.getElementById('ETH_BTC_CLOSED').style.transition = 'ease';
                        document.getElementById('ETH_BTC_CLOSED').style.color = '#ff4c4c';
                    } else {
                        document.getElementById('ETH_BTC_CLOSED').style.color = '#77CB8E';
                    }
                }
            }
            if (res.PRICE) {
                document.getElementById('ETH_BTC_CLOSED').textContent = 'Ƀ ' + res.PRICE.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
                _oldPriceThree = res.PRICE;
            }
            if (res.VOLUME24HOURTO) {
                document.getElementById('ETC_BTC_VOLUME').textContent = 'V: Ƀ ' + res.VOLUME24HOURTO.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
            }
        }

        if (res.FROMSYMBOL == "ZEC" && res.TOSYMBOL == "BTC") {
            if (res.PRICE) {
                if (_oldPriceFour != 0) {

                    if (_oldPriceFour > res.PRICE) {
                        document.getElementById('ZEC_BTC_CLOSED').style.transition = 'ease';
                        document.getElementById('ZEC_BTC_CLOSED').style.color = '#ff4c4c';
                    } else {
                        document.getElementById('ZEC_BTC_CLOSED').style.color = '#77CB8E';
                    }
                }
            }
            if (res.PRICE) {
                document.getElementById('ZEC_BTC_CLOSED').textContent = 'Ƀ ' + res.PRICE.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
                _oldPriceFour = res.PRICE;
            }
            if (res.VOLUME24HOURTO) {
                document.getElementById('ZEC_BTC_VOLUME').textContent = 'V: Ƀ ' + res.VOLUME24HOURTO.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
            }
        }

        if (res.FROMSYMBOL == "BTC" && res.TOSYMBOL == "USD") {
            if (res.PRICE) {
                if (_oldPriceFive != 0) {

                    if (_oldPriceFive > res.PRICE) {
                        document.getElementById('BTC_USD_CLOSED').style.transition = 'ease';
                        document.getElementById('BTC_USD_CLOSED').style.color = '#ff4c4c';
                    } else {
                        document.getElementById('BTC_USD_CLOSED').style.color = '#77CB8E';
                    }
                }
            }
            if (res.PRICE) {
                document.getElementById('BTC_USD_CLOSED').textContent = '$ ' + res.PRICE.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
                _oldPriceFive = res.PRICE;
            }
            if (res.VOLUME24HOURTO) {
                document.getElementById('BTC_USD_VOLUME').textContent = 'V: Ƀ ' + res.VOLUME24HOURTO.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
            }
        }

        if (res.FROMSYMBOL == "DASH" && res.TOSYMBOL == "BTC") {
            if (res.PRICE) {
                if (_oldPriceSix != 0) {

                    if (_oldPriceSix > res.PRICE) {
                        document.getElementById('DASH_BTC_CLOSED').style.transition = 'ease';
                        document.getElementById('DASH_BTC_CLOSED').style.color = '#ff4c4c';
                    } else {
                        document.getElementById('DASH_BTC_CLOSED').style.color = '#77CB8E';
                    }
                }
            }
            if (res.PRICE) {
                document.getElementById('DASH_BTC_CLOSED').textContent = 'Ƀ ' + res.PRICE.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
                _oldPriceSix = res.PRICE;
            }
            if (res.VOLUME24HOURTO) {
                document.getElementById('DASH_BTC_VOLUME').textContent = 'V: Ƀ ' + res.VOLUME24HOURTO.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
            }
        }
    }
});

function updateData(res, svgdoc) {
    if (res.PRICE) {
        svgdoc.getElementById("price").textContent = "$ " + res.PRICE.toLocaleString();

        if (svgdoc.oldPrice > res.PRICE) {
            svgdoc.getElementById("price").style.transition = "fill 10ms";
            svgdoc.getElementById("price").style.fill = "#FF0000";
            //svgdoc.getElementById("footer").style.fill = "darkred";
            setTimeout(function () {
                svgdoc.getElementById("price").style.transition = "fill 4000ms";
                svgdoc.getElementById("price").style.fill = "#000000";
            }, 100);
        } else {
            svgdoc.getElementById("price").style.transition = "fill 10ms";
            svgdoc.getElementById("price").style.fill = "#00FF00";
            //svgdoc.getElementById("footer").style.fill = "green";
            setTimeout(function () {
                svgdoc.getElementById("price").style.transition = "fill 4000ms";
                svgdoc.getElementById("price").style.fill = "#000000";
            }, 100);
        }
        svgdoc.getElementById("price").style.fontWeight = "700";

        svgdoc.oldPrice = res.PRICE;
    }
    if (res.VOLUME24HOURTO) {

        if (svgdoc.oldVolume > res.VOLUME24HOURTO) {
            svgdoc.getElementById("volume").style.transition = "fill 10ms";
            svgdoc.getElementById("volume").style.fill = "#FF0000";
            setTimeout(function () {
                svgdoc.getElementById("volume").style.transition = "fill 4000ms";
                svgdoc.getElementById("volume").style.fill = "#000000";
            }, 100);
        } else {
            svgdoc.getElementById("volume").style.transition = "fill 10ms";
            svgdoc.getElementById("volume").style.fill = "#00FF00";
            setTimeout(function () {
                svgdoc.getElementById("volume").style.transition = "fill 4000ms";
                svgdoc.getElementById("volume").style.fill = "#000000";
            }, 100);
        }
        svgdoc.getElementById("basePrice").textContent ="B "  +(res.VOLUME24HOUR - svgdoc.oldVolume24).toFixed(6);
        svgdoc.getElementById("volume").textContent = "Vol: " + res.VOLUME24HOURTO.toLocaleString();
        svgdoc.getElementById("volume").style.textAnchor = "end";
        svgdoc.oldVolume = res.VOLUME24HOURTO;
        svgdoc.oldVolume24 = res.VOLUME24HOUR;
    }
}
function BTC(res) {
    updateData(res, document.getElementById("btcUsd").contentDocument);
}
function ETH(res) {
    updateData(res, document.getElementById("ethUsd").contentDocument);
}
function DASH(res) {
    updateData(res, document.getElementById("dashUsd").contentDocument);
}
function ZEC(res) {
    updateData(res, document.getElementById("zecUsd").contentDocument);
}

//getGraphData();
